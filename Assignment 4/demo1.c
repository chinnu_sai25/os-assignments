#include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <pthread.h>
    #include <unistd.h>  // sleep()
    #include <stdio.h>
    #include <stdlib.h>  // EXIT_SUCCESS
    #include <string.h>  // strerror()
    #include <errno.h>

    #define SIZE_OF_DATASET 6
    void* quickSort( void* data);
    int partition( int* a, int, int);


    struct info {
        int start_index;
        int* data_set;
        int end_index;
    };



   int main(int argc, char **argv)
{

    int a[] = { 7, 12, 1, -2, 0, 15, 4, 11, 9,5,3,24,5,23,3,1,56,8,4,34,23,51};
    struct info *info = malloc(sizeof(struct info));
    info->data_set=malloc(sizeof(int)*SIZE_OF_DATASET);
    info->data_set=a;
    info->start_index=0;
    info->end_index=SIZE_OF_DATASET-1;

    quickSort(info);
    printf("\n\nSorted array is:  ");
    int i;
      for(i = 0; i < SIZE_OF_DATASET; ++i)
           printf(" %d ", info->data_set[i]);
    return 0;
}

// void* quickSort( void *data)
// {
//     struct info *info = data;
//     struct info *info1 = data;
//     int j,l,r;
//     l = info->start_index;
//     r = info->end_index;

//     pthread_attr_t attr;
//     pthread_t thread_id1;
//     pthread_attr_init(&attr);

//    if( l < r )
//    {

//        j = partition( info->data_set, l, r);
//        info1->start_index=j+1;
//        info1->end_index=r;
//        info1->data_set = info->data_set;
//        if(info1->end_index<0)info1->end_index=0;

//       if (pthread_create(&thread_id1, NULL, quickSort, info1)) {
//             fprintf(stderr, "No threads for you.\n");
//             return NULL;
//       }
//       info->start_index=l;
//       info->end_index=j-1;

//       if(info->end_index < 0) info->end_index = 0;
//       quickSort(info);  /* don't care about the return value */
//       pthread_join(thread_id1, NULL);

//   }

// return NULL;

// }
void* quickSort( void *data)
{
    struct info *info = data;
    struct info other_info;

    /* ... */
     int j,l,r;
    l = info->start_index;
    r = info->end_index;

    pthread_attr_t attr;
    pthread_t thread_id1;
    pthread_attr_init(&attr);

   if( l < r )
   {

       j = partition( info->data_set, l, r);

        /* launch a new thread to handle one partition: */
        other_info.start_index = j + 1;
        other_info.end_index = r;
        other_info.data_set = info->data_set;
        if (pthread_create(&thread_id1, NULL, quickSort, &other_info)) {
            fprintf(stderr, "No threads for you.\n");
            return NULL;
        }

        /* handle the other partition in the current thread: */
        info->start_index = l;
        info->end_index = j - 1;
        if(info->end_index < 0) info->end_index = 0;
        quickSort(info);  /* don't care about the return value */

        /* after this thread is done, wait for the other thread to finish, too */
        pthread_join(thread_id1, NULL);
   }
            return NULL;

    /* ... */
}

    int partition( int* a, int l, int r) {
       int pivot, i, j, t;
       pivot = a[l];
       i = l; j = r+1;

       while( 1)
       {
        do ++i; while( a[i] <= pivot && i <= r );
        do --j; while( a[j] > pivot );
        if( i >= j ) break;
        t = a[i]; a[i] = a[j]; a[j] = t;
       }
       t = a[l]; a[l] = a[j]; a[j] = t;
       return j;
    }
