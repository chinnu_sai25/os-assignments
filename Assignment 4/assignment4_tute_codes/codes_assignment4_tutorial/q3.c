#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>
#include <semaphore.h>
#include<errno.h>

#define POOL 1
#define PREMIER 2

pthread_t riders_threads[1000];
pthread_t drivers_threads[1000];
int totaldrivers;
int totalservers;
int riderno[1000];
int driverno[1000]; //0 indicates driver is free,1 indicates he can accept one more,2 indicates he cant accept anymore 
// waiting for cab limits no.of riders from booking a cab
int paymentno[1000]; // 0 indicates that index server is free else busy
sem_t waitingforcab;
sem_t waiting_pool;
sem_t drivers_semaphores[1000];
sem_t payments_semaphore;
typedef struct{
	int cabtype;
	int maxwaittime; //maximum 5
	int ridetime; //maximum 5
	int riderid;
}cab;
typedef struct 
{
	int cabtype;
	int ridetime;
	int driverid;
	int cab_rider1;
	int cab_rider2;
}driver;
typedef struct 
{
	int riderid;
}payment;

int pool_it;
int current_driver_id=-1;

void *make_payment(void *input){
	payment *args = (payment*)input;
	int riderid = args->riderid;
	sem_wait(&payments_semaphore);
	sleep(2);
	printf("payment is done by rider %d\n",riderid);
	sem_post(&payments_semaphore);
}
void *driver_ride(void *input){
	driver *args= (driver*)input;
	int cabtype = args->cabtype;
	int ridetime = args->ridetime;
	int driverid = args->driverid;
	int cab_rider1 = args->cab_rider1;
	int cab_rider2 = args->cab_rider2;
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
	{
    	return NULL;
	}
	int s;
	ts.tv_sec += ridetime;
	if(cabtype==2){
		sem_wait(&drivers_semaphores[driverid]); // need to wait for ts seconds and then need to decrease semaphore value
		sleep(ridetime);
		printf("Rider %d of PRIMER is done with his ride\n", cab_rider1);
		driverno[cab_rider1]=0;
		sem_post(&drivers_semaphores[driverid]);
		sem_post(&waitingforcab);
		payment arguments;
		arguments.riderid = cab_rider1;
		pthread_t payments_threads;
		pthread_create(&payments_threads,NULL,make_payment,&arguments);
		sleep(0.5);
	}
	else if(cabtype==1 && cab_rider2==-1){
		printf("a-%d\n",driverid);
		sem_wait(&drivers_semaphores[driverid]); // need to wait for ts seconds and then need to decrease semaphore value
		printf("b-%d\n",driverid);
		sleep(ridetime);
		printf("Rider %d of POOL is done with his ride\n", cab_rider1);
		if(driverno[driverid]==1){
			driverno[driverid]=0;
			sem_post(&drivers_semaphores[driverid]);
			sem_post(&waitingforcab);
			printf("This cab is free\n");
		}
		payment arguments;
		arguments.riderid = cab_rider1;
		pthread_t payments_threads;
		pthread_create(&payments_threads,NULL,make_payment,&arguments);
		sleep(0.5);
	}
	else if(cabtype==1 && cab_rider1==-1){
		sleep(ridetime);
		printf("Rider %d of POOL is done with his ride\n", cab_rider2);
		driverno[cab_rider1]=0;
		sem_post(&drivers_semaphores[driverid]);
		sem_post(&waitingforcab);
		payment arguments;
		arguments.riderid = cab_rider2;
		pthread_t payments_threads;
		pthread_create(&payments_threads,NULL,make_payment,&arguments);
		sleep(0.5);
	}
}
void *bookcab(void *input){
	cab *args= (cab*)input;
	int cabtype = args->cabtype;
	int maxwaittime = args->maxwaittime;
	int ridetime = args->ridetime;
	int riderid = args->riderid;
	if(cabtype==1){
		if(pool_it==1){
			pool_it=0;
		}
		else if(pool_it==0){
			pool_it=1;
			// sem_wait(&waiting_pool);
		}
		printf("Rider %d trying to find a cab of type POOL\n",riderno[riderid]);
	}
	else{
		printf("Rider %d trying to find a cab of type PREMIER\n",riderno[riderid]);
	}
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == -1)
	{
    	return NULL;
	}
	int s;
	ts.tv_sec += maxwaittime;
	int k = (cabtype==1 && pool_it==0);
	if(!k)
	{
		while ((s = sem_timedwait(&waitingforcab,&ts)) == -1 && errno == EINTR)
            continue;  
		if (s == -1)
		{
    		if (errno == ETIMEDOUT)
        		printf("Rider %d exits from the system as he waited till he reaches maxWaitTime\n",riderno[riderid]);
    	else
        	perror("sem_timedwait");
		} 	
		else
		{
			if(cabtype==1){
				int i=0;
				for(i=0;i<totaldrivers-1;i++){
					if(driverno[i]==0){break;}
				}
				driverno[i]=1;
				printf("Rider %d has succesfully booked a cab of type POOL\n",riderno[riderid]);
				driver args;
				args.cabtype=1;
				args.ridetime=ridetime;
				args.driverid=i;
				args.cab_rider1=riderid;
				args.cab_rider2=-1;
				pthread_create(&drivers_threads[i],NULL,driver_ride,&args);
				sleep(1);
			}
			else{
				int i=0;
				for(i=0;i<totaldrivers-1;i++){
					if(driverno[i]==0){break;}
				}
				driverno[i]=2;
				printf("Rider %d has succesfully booked a cab of type PREMIER\n",riderno[riderid]);
				driver args;
				args.cabtype=2;
				args.ridetime=ridetime;
				args.driverid=i;
				args.cab_rider1=riderid;
				args.cab_rider2=riderid;
				pthread_create(&drivers_threads[i],NULL,driver_ride,&args);
				sleep(1);
			}
		}
	}
	else{
		// pool_it=0;
			int i=0;
			for(i=0;i<totaldrivers-1;i++){
				if(driverno[i]==1){break;}
			}
			driverno[i]=2;
			printf("Rider %d has succesfully booked a cab of type POOL\n",riderno[riderid]);
			driver args;
			args.cabtype=1;
			args.ridetime=ridetime;
			args.driverid=i;
			args.cab_rider1=-1;
			args.cab_rider2=riderid;
			pthread_create(&drivers_threads[i],NULL,driver_ride,&args);
			sleep(1);
	}
}
int main(){
	int N,M,K; //N cabs, M riders and K payment servers
	printf("Enter N (cabs), M (riders) and K (payment servers)\n");
	scanf("%d %d %d",&N,&M,&K);
	
	totaldrivers = N;
	totalservers = K;

	sem_init(&waitingforcab, 0, N); // initializing semaphore waitingforcab with N as all cab drivers are initially free and ready to accept a ride
	sem_init(&waiting_pool, 0, 2); // initializing semaphore waitingforcab with N as all cab drivers are initially free and ready to accept a ride
	sem_init(&payments_semaphore, 0, K);
	for(int i=0;i<N;i++){
		sem_init(&drivers_semaphores[i],0,1);
	}
	for(int i=0;i<M;i++){
		cab args;
		args.cabtype = rand()%2+1;
		args.maxwaittime = rand()%5+1;
		args.ridetime = rand()%5+1;
		args.riderid=i;
		riderno[i]=i;
		pthread_create(&riders_threads[i],NULL,bookcab,&args);
		sleep(2);
	}
	for(int i=0;i<N;i++){
		pthread_join(drivers_threads[i],NULL);
		sleep(1);
	}
	for(int i=0;i<M;i++){
		pthread_join(riders_threads[i],NULL);
		sleep(1);
	}
	
	// for(int i=0;i<K;i++){
	// 	pthread_join(payments_threads[i],NULL);
	// 	sleep(1);
	// }
