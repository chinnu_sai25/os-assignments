#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>
int biryani_vessels[10000];// biryani_vessel[i] is the no.of biryani vessels prepared by chef i
int serving_tables[10000];//non zero indicates that number of biryani vessels are loaded 0 indicates that repective serving table is empty
int no_of_slots[1000];//no_of_slots[i] indicates the no.of slots assigned for serving table i
int vesselnumber;
int time_to_preare_biryani_vessels[10000];// time_to_preare_biryani_vessels[i] is the time to prepare biryani_vessels[i] no.of vessels prepared by chef i
int students_can_be_served[1000];// students_can_be_served[i] indicates p students that can be served by vessel i

pthread_t chef_pthreads[1000];
pthread_t serving_tables_pthreads[1000];
pthread_t students_pthreads[1000];

int remaning_no_of_students;
int N;//No.of serving tables
int K;//No.of students
pthread_mutex_t mutex[1000];
typedef struct  
{
	int chef_id;
}chefno;
typedef struct  
{
	int t;
}slotsno;
typedef struct 
{
	int servingtable_id;
}servingcntrno;
typedef struct 
{
	int student_id;
}studentsno;

void ready_to_serve_table(int servingtable_id){
		
	int servingslots=rand()%10+1;
	pthread_mutex_lock(&mutex[servingtable_id]);
	if(servingslots<serving_tables[servingtable_id]){
		no_of_slots[servingtable_id]=servingslots;
	}
	else{
		no_of_slots[servingtable_id]=serving_tables[servingtable_id];
	}
	printf("Current slots of serving table %d are %d\n",servingtable_id,no_of_slots[servingtable_id]);
	serving_tables[servingtable_id]=serving_tables[servingtable_id]-no_of_slots[servingtable_id];
	pthread_mutex_unlock(&mutex[servingtable_id]);

	while(no_of_slots[servingtable_id]!=0 ){
	}
	return;
}
void *pre_ready_to_serve_table(void *input){
	while(1){
		servingcntrno *args = (servingcntrno*)input;
		int servingtable_id = args->servingtable_id;
		if(serving_tables[servingtable_id]){
		while(serving_tables[servingtable_id]!=0){
		ready_to_serve_table(servingtable_id);
		sleep(2);
		}
		}
	}
}
int student_in_slot(){
	sleep(3);
}																					// hANDLING CHefs need to prepare after their vessels done
void *wait_for_slot(void *input){													// P students????????			
	studentsno *args = ( studentsno*) input;
	printf("student %d waiting for slot\n",args->student_id);
	int student_id = args->student_id;
	int flag=0;
	while(1){
	int i;
	for(i=0;i<N;i++){
		if(pthread_mutex_trylock(&mutex[i]))continue;
		if(no_of_slots[i]!=0){
			no_of_slots[i]--;
			remaning_no_of_students--;
			printf("student %d had done eating from serving table %d\n",student_id,i);
			flag=1;
			break;
		}
		pthread_mutex_unlock(&mutex[i]);
	}
	if(flag==1)
		{
			pthread_mutex_unlock(&mutex[i]);
			break;
		}
		else{continue;}
	}
	student_in_slot();
}

void biryani_ready(int input){
	int chef_id = input;
	while(biryani_vessels[chef_id]!=0){
		int i=0;
		while(1)
		{
			for(i=0;i<N;i++)
			{
				if(serving_tables[i]==0){
					break;
				}
			}
			if(i<N)
			{
				pthread_mutex_lock(&mutex[i]);
				// serving_tables[i]=rand()%26+25;
				serving_tables[i]=rand()%2+2;
				printf("Chef %d allocated one of his vessel to serving table %d which has a capacity to serve %d students\n",chef_id,i,serving_tables[i]);
		    	pthread_mutex_unlock(&mutex[i]);
				biryani_vessels[chef_id]--;
		    	break;
	    	}
		}
	}
	return;
}

void *biryani_make(void *input){
	while(1){
	chefno *args = ( chefno*) input;
	int chef_id = args->chef_id;
	biryani_vessels[chef_id]=rand()%10+1;// r according to given question
	time_to_preare_biryani_vessels[chef_id]=rand()%4+2; // w according to given question
	printf("Chef %d is preparing %d no.of vessels\n",chef_id,biryani_vessels[chef_id]);
	sleep(time_to_preare_biryani_vessels[chef_id]);
	biryani_ready(chef_id);
	}
}

int main(){
	int M;
	printf("Enter the no.of robot chefs(i.e M)\n");
	scanf("%d",&M);
	N;
	printf("Enter the no.of serving tables(i.e N)\n");
	scanf("%d",&N);

	printf("Enter the no.of students(i.e K)\n");
	scanf("%d",&K);
	
	remaning_no_of_students=K;

	for(int i=0;i<N;i++){
		pthread_mutex_init(&mutex[i],NULL);
	}
	for(int i=0;i<M;i++){
		chefno args;
		args.chef_id=i;
		pthread_create(&chef_pthreads[i],NULL,biryani_make,&args);
		sleep(2);
	}

	for(int i=0;i<N;i++){
		servingcntrno args;
		args.servingtable_id=i;
		pthread_create(&serving_tables_pthreads[i],NULL,pre_ready_to_serve_table,&args);
		sleep(2);
	}

	for(int i=0;i<K;i++){
		studentsno args;
		args.student_id=i;
		pthread_create(&students_pthreads[i],NULL,wait_for_slot,&args);
		sleep(1);
	}

	for(int i=0;i<K;i++){
		pthread_join(students_pthreads[i],NULL);
		sleep(2);
	}
	
	
	for(int i=0;i<N;i++){
		printf("Capacity at serving_table %d are %d\n",i,serving_tables[i] );
	}
	for(int i=0;i<N;i++){
		printf("No of slots left at serving_table %d are %d\n",i,no_of_slots[i] );
	}

}