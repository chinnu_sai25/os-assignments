#define _POSIX_C_SOURCE 199309L //required for clock
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>

int rand_gen(){
    srand(time(0));
    return rand();
}
// Macro for swapping two values.
#define SWAP(x,y) do {\
    __typeof__(x) tmp = x;\
    x = y;\
    y = tmp;\
} while(0)

// Insertion sort for performing insertion sort if number of elements are less than 5
void insertion(int arr_here[],int a,int b){
    int i,j;
    for(i=a+1;i<=b;i++){
        int value=arr_here[i];
        for(j=i-1;j>=a;j--){
            if(arr_here[j]>value){
                arr_here[j+1]=arr_here[j];
            }
            else{
                break;
            }
        }
        arr_here[j+1]=value;
    }
}

// Creating a shared memory 
int * shareMem(size_t size){
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (int*)shmat(shm_id, NULL, 0);
}

// Partition function which partitions the given array into two halves where all the elements to the left are smaller
//  than pivot and all elements to the right are larger than pivot , and returns the index of pivot element
int partition(int *array, int left, int right, int pivot)
{
    int pivotValue = array[pivot];
    SWAP(array[pivot], array[right]);
    int storeIndex = left;
    for (int i=left ; i<right ; i++)
    {
        if (array[i] <= pivotValue)
        {
            SWAP(array[i], array[storeIndex]);
            storeIndex++;
        }
    }
    SWAP(array[storeIndex], array[right]);
    return storeIndex;
}

void normal_quicksort(int *arr, int l, int r){
    if(l>r) return;
    int left=l,right=r;

    if (right-left+1<=5)
    {						// If number of elements are less than 5 then insertion sort is performed
       insertion(arr,left,right);
       return;
    }
    if (right > left)							// If number of elements are greater than 5 performing quick sort
    {
       int pivotIndex = left + (right - left)/2;
       pivotIndex = partition(arr, left, right, pivotIndex);
       normal_quicksort(arr, left, pivotIndex-1);
       normal_quicksort(arr, pivotIndex+1, right);
    }
}

void concurrent_quicksort(int *arr, int l, int r){
    if(l>r) _exit(1);    
    int left=l,right=r;

    if (right-left+1<=5)
    {						// If number of elements are less than 5 then insertion sort is performed
       insertion(arr,left,right);
       return;
    }
    int pivotIndex = rand_gen()%(r-l+1)+(l);
    pivotIndex = partition(arr, left, right, pivotIndex);
    
    int pid1 = fork();
    int pid2;
    if(pid1==0){
    concurrent_quicksort(arr, left, pivotIndex-1);
        _exit(1);
    }
    else{
        pid2 = fork();
        if(pid2==0){
            concurrent_quicksort(arr, pivotIndex+1, right);
            _exit(1);
        }
        else{
            int status;
            waitpid(pid1, &status, 0);
            waitpid(pid2, &status, 0);
        }
    }
    return;
}

struct arg{
    int l;
    int r;
    int* arr;    
};

void *threaded_quicksort(void* a){
    //note that we are passing a struct to the threads for simplicity.
    struct arg *args = (struct arg*) a;

    int l = args->l;
    int r = args->r;
    int *arr = args->arr;
    int left=l,right=r;

    if(l>r) return NULL;   

    if (right-left+1<=5)
    {						// If number of elements are less than 5 then insertion sort is performed
       insertion(arr,left,right);
       return NULL;
    }
 
    int pivotIndex = left + (right - left)/2;
    pivotIndex = partition(arr, left, right, pivotIndex);
    //sort left half array
    struct arg a1;
    a1.l = l;
    a1.r = pivotIndex-1;
    a1.arr = arr;
    pthread_t tid1;
    pthread_create(&tid1, NULL, threaded_quicksort, &a1);
    
    //sort right half array
    struct arg a2;
    a2.l = pivotIndex+1;
    a2.r = r;
    a2.arr = arr;
    pthread_t tid2;
    pthread_create(&tid2, NULL, threaded_quicksort, &a2);
    
    //wait for the two halves to get sorted
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    // return NULL;
}

void runSorts(long long int n){

    struct timespec ts;
    
    //getting shared memory
    int *arr = shareMem(sizeof(int)*(n+1));
    printf("Enter the %d elements\n",n );
    for(int i=0;i<n;i++) scanf("%d", arr+i);
    int brr[n+1];
    int crr[n+1];
    for(int i=0;i<n;i++) brr[i] = arr[i];
    for(int i=0;i<n;i++) crr[i] = arr[i];

    printf("Running concurrent_quicksort for n = %lld\n", n);
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    long double st = ts.tv_nsec/(1e9)+ts.tv_sec;

    // for(int i=0;i<n;i++){
    //     printf("%d\n",arr[i] );
    // }
    //multiprocess quicksort
    concurrent_quicksort(arr, 0, n-1);
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    long double en = ts.tv_nsec/(1e9)+ts.tv_sec;
    printf("time = %Lf\n", en - st);
    // for(int i=0;i<n;i++){
    //     printf("%d\n",arr[i] );
    // }
    long double t1 = en-st;

    pthread_t tid;
    struct arg a;
    a.l = 0;
    a.r = n-1;
    a.arr = brr;
    printf("Running threaded_concurrent_quicksort for n = %lld\n", n);
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    st = ts.tv_nsec/(1e9)+ts.tv_sec;

    // for(int i=0;i<n;i++){
    //     printf("%d\n",brr[i] );
    // }
    //multithreaded quicksort
    pthread_create(&tid, NULL, threaded_quicksort, &a);
    pthread_join(tid, NULL);    

    // for(int i=0;i<n;i++){
    //     printf("%d\n",brr[i] );
    // }
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    en = ts.tv_nsec/(1e9)+ts.tv_sec;
    printf("time = %Lf\n", en - st);
    long double t2 = en-st;

    printf("Running normal_quicksort for n = %lld\n", n);
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    st = ts.tv_nsec/(1e9)+ts.tv_sec;

    // for(int i=0;i<n;i++){
    //     printf("%d\n",crr[i] );
    // }
    // normal quicksort
    normal_quicksort(crr, 0, n-1);    
    // for(int i=0;i<n;i++){
    //     printf("%d\n",crr[i] );
    // }
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    en = ts.tv_nsec/(1e9)+ts.tv_sec;
    printf("time = %Lf\n", en - st);
    long double t3 = en - st;

    printf("normal_quicksort ran:\n\t[ %Lf ] times faster than concurrent_quicksort\n\t[ %Lf ] times faster than threaded_concurrent_quicksort\n\n\n", t1/t3, t2/t3);
    shmdt(arr);
    return;
}

int main(){

    long long int n;
    printf("Enter total no.of elements\n");
    scanf("%lld", &n);
    runSorts(n);
    return 0;
}
