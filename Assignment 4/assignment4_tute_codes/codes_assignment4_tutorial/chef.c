#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>
int biryani_vessels[10000];// biryani_vessel[i] is the no.of biryani vessels prepared by chef i
// int no_of_students[10000];//
int serving_tables[10000];//1 indicates a biryani vessel is loaded 0 indicates the 
int time_to_preare_biryani_vessels[10000];// time_to_preare_biryani_vessels[i] is the time to prepare biryani_vessels[i] no.of vessels prepared by chef i
pthread_t chef_pthreads[1000];
pthread_t serving_tables_pthreads[1000];
pthread_t students_pthreads[1000];
int remaning_no_of_students;
int empty_containers=0;
int N;//No.of serving tables
int K;//No.of students
pthread_mutex_t mutex;
typedef struct  
{
	int t;
}chefno;
typedef struct  
{
	int t;
}slotsno;
typedef struct 
{
	int t;
}servingcntrno;
typedef struct 
{
	int t;
}studentsno;
void *ready_to_serve_table(void *input){
	//The function must not return until either all the serving slots of the serving table are full or all the waiting students have been assigned a slot
    slotsno *args = ( slotsno*) input;
	int k = args->t;
	if(remaning_no_of_students>=k){
		remaning_no_of_students=remaning_no_of_students-k;
		printf("rem%d\n",remaning_no_of_students );
		sleep(2);
		return NULL;
	}
	else{
		remaning_no_of_students=0;
		sleep(2);
		return NULL;
	}

}
void *student_in_slot(void *input){
    servingcntrno *args = ( servingcntrno*) input;
	int k = args->t;
	if(serving_tables[k]==0){
		serving_tables[k]=1;
		return NULL;
	}
}
void *wait_for_slot(void *input){
	studentsno *args = ( studentsno*) input;
	int k = args->t;
	int flag=0;
	int i;
	for(i=0;i<N;i++){
		if(serving_tables[i]==0){
			flag=1;
			break;
		}
	}
	if(flag==1){
	servingcntrno args;
	args.t = i;	
	pthread_create(&students_pthreads[k],NULL, student_in_slot,&args);
	return NULL;
	}
}
int students(int k){
	for(int i=0;i<k;i++){
		studentsno args;
		args.t=k;
		pthread_create(&students_pthreads[i],NULL,wait_for_slot,&args);
	}
}
int biryani_ready(int k){
	// printf("r-%d\n",k);
	for(int i=0;i<N;i++){
		if(serving_tables[i]==0){
			empty_containers++;
		}
	}
	// printf("ec-%d\n",empty_containers);
	int arr[1000];
	int end=0;
	// if(k>empty_containers){
		//donot return

	// }
	while(k!=0){
		int i=0;
		for(i=0;i<N-1;i++){
			if(serving_tables[i]==0){
				break;
			}
		}
		pthread_mutex_lock(&mutex);
		serving_tables[i]=1;
		k--;
		empty_containers--;
    	pthread_mutex_unlock(&mutex);
	}
	return;
}
void *biryani_make(void *input){
	chefno *args = ( chefno*) input;
	int chef_id = args->t;
	biryani_vessels[chef_id]=rand()%10+1;// r according to given question
	time_to_preare_biryani_vessels[chef_id]=rand()%4+2; // w according to given question
	biryani_ready(biryani_vessels[chef_id]);
	sleep(2);
	return NULL;
}
void chefs(int k){
	for(int i=0;i<k;i++){
		chefno args;
		args.t=k;
		pthread_create(&chef_pthreads[i],NULL,biryani_make,&args);
	}
	for(int i=0;i<k;i++){
		pthread_join(chef_pthreads[i],NULL);
	}
	return;
}
int main(){
	int M=20;
	printf("Enter the no.of robot chefs(i.e M)\n");
	scanf("%d",&M);
	N = 20;
	printf("Enter the no.of serving tables(i.e N)\n");
	scanf("%d",&N);

	printf("Enter the no.of students(i.e K)\n");
	scanf("%d",&K);
	remaning_no_of_students=K;
	chefs(M);
	students(K);

	for(int i=0;i<N;i++){
		printf("%d\n",serving_tables[i] );
	}
}