#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include"sys/wait.h"
#include"sys/types.h"
#include<sys/stat.h>
#include<fcntl.h>
#include<dirent.h>
#include<time.h>
#include<pwd.h>
#include<grp.h>
// cd unexist dir
// pinfo ~ visible
// cd --- seg fault

char temp_inp[500];//for echo purpose
void builtin_cd(char** arguments,char *username,char *systemname,char *home){
    if(strcmp(arguments[0],"cd")==0){
        if(strcmp(arguments[1],".")==0){
            return;
        }
        else if(strcmp(arguments[1],"~")==0){
            chdir(home);
            return;
        }
        else{
            chdir(arguments[1]);
            return;
        }
    }
}
char **split_input(char *input,int stage){
strcpy(temp_inp,input);
char *delimiter;
int bufsize = 10;
char **total_inputs = malloc(bufsize*sizeof(char*));
char *current_input;
int it = 0;
if(!total_inputs){
    perror("Memory allocation error");
    exit(1);
}
if(stage==1){
    current_input = strtok(input,";");}
else if(stage==2){
    current_input = strtok(input," \t\r\n\a");}
while(current_input != NULL){
    total_inputs[it++] = current_input;
    if (it >= bufsize) {
      bufsize += 50;
      total_inputs = realloc(total_inputs, bufsize * sizeof(char*));
      if (!total_inputs) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
    if(stage==1){
    current_input = strtok(NULL,";");}
    else if(stage==2){
    current_input = strtok(NULL," \t\r\n\a");}
}
    total_inputs[it]=NULL;
    return total_inputs;
}
void ls_cmd(char *directory,char **arguments){
DIR *entered_dir;
int l_used = 0;
struct dirent *file_iterator;
struct stat file_info;
entered_dir = opendir(directory);
stat(directory,&file_info);
while((file_iterator=readdir(entered_dir))!=NULL){
    stat(file_iterator->d_name,&file_info);
    if(strcmp(arguments[1],"-l")==0){
        l_used = 1;
        printf( (S_ISDIR(file_info.st_mode)) ? "d" : "-");
        printf( (file_info.st_mode & S_IWUSR) ? "w" : "-");
        printf( (file_info.st_mode & S_IRUSR) ? "r" : "-");
        printf( (file_info.st_mode & S_IXUSR) ? "x" : "-");
        printf( (file_info.st_mode & S_IRGRP) ? "r" : "-");
        printf( (file_info.st_mode & S_IWGRP) ? "w" : "-");
        printf( (file_info.st_mode & S_IXGRP) ? "x" : "-");
        printf( (file_info.st_mode & S_IROTH) ? "r" : "-");
        printf( (file_info.st_mode & S_IWOTH) ? "w" : "-");
        printf( (file_info.st_mode & S_IXOTH) ? "x" : "-"); 
        printf(" ");  
        FILE *fp;
        char path[1500];
        char links[500] = "stat "; 
        strcat(links,file_iterator->d_name); 
        strcat(links," | grep Links: ");
        fp = popen(links, "r");
        if (fp == NULL) {
            printf("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path, sizeof(path)-1, fp) != NULL) {
        }
        char ** links_number = split_input(path,2);
        printf("%s ",links_number[5]);
        pclose(fp);
     
        char path1[1500];
        char user[500] ="stat -c '%U %G' ";
        strcat(user,file_iterator->d_name); 
        fp = popen(user, "r");
        if (fp == NULL) {
          printf("Failed to run command\n" );
          exit(1);
        }
        while (fgets(path1, sizeof(path1)-1, fp) != NULL) {
          // printf("%s", path1);
        }
        int i=0;
        for(i=0;path1[i]!='\n';i++){}
        path1[i]='\0';
        printf("%s  ", path1);
        pclose(fp);

        char path2[1500];
        char size[500] = "stat "; 
        strcat(size,file_iterator->d_name); 
        strcat(size," | grep Size: ");
        fp = popen(size, "r");
        if (fp == NULL) {
            printf("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path2, sizeof(path2)-1, fp) != NULL) {
        }
        char ** size_number = split_input(path2,2);
        printf("%s ",size_number[1]);
        pclose(fp);
    }
    else if(strcmp(arguments[1],"-a")!=0){
        if(file_iterator->d_name[0] == '.'){}
        else{
            printf("%s ",file_iterator->d_name);
        }
    }
    else{
        printf("%s  ",file_iterator->d_name);
    }
if(l_used==1)printf("\n");
}
if(l_used==0)printf("\n");
}
void shell_loop(){
char username[500],systemname[500],curr_dir[500],home[500],print_cwd[500];
char *input=NULL;
char **arguments;
int it_chdir = -1;
size_t input_length;
ssize_t input_read;
int home_status=0,check_home_currdir;
while(1){
cuserid(username);
gethostname(systemname,sizeof(systemname));
getcwd(curr_dir,sizeof(curr_dir));
if(home_status == 0){
strcpy(home,curr_dir);
home_status = 1;
}
check_home_currdir = strcmp(home,curr_dir);
if(check_home_currdir != 0){
    for(int i=0;home[i]!='\0';i++){
        if(home[i]!=curr_dir[i])
        {
            perror("Working in a directory which is not accessible");
            // return;
            break;
        }
            it_chdir = i;

    }
            if(it_chdir!=-1){
            int count = 1;
            // strcpy(print_cwd,"0");//
            print_cwd[0]='~';
            for(int j=it_chdir+1;curr_dir[j]!='\0';){
                print_cwd[count++] = curr_dir[j++];
            }
            print_cwd[count]='\0';
            it_chdir=-1;
            }
}
else{
    print_cwd[0]='~';
    print_cwd[1]='\0';
}
printf("<%s@%s:%s>",username,systemname,print_cwd);
input_read = getline(&input,&input_length,stdin);
arguments = split_input(input,2);
if(input_read!=-1){
    if(strcmp(arguments[0],"cd")==0){
            builtin_cd(arguments,username,systemname,home);
            // strcpy(print_cwd,"");//
            // printf("cc--%s\n",print_cwd );//
        }
    else if(strcmp(arguments[0],"pwd")==0){
        printf("%s\n",curr_dir);
    }
    else if(strcmp(arguments[0],"echo")==0){
        int temp_flag = 0;
        for(int i=0;temp_inp[i]!='\0';i++){
            if(temp_flag==1 && temp_inp[i]!='"'){
                printf("%c",temp_inp[i]);
            }
            if(temp_inp[i]=='"'){
                temp_flag=1;
            }
        }
    }
    else if(strcmp(arguments[0],"pinfo")==0){
        char *s;
        char arr[500];
        long int current_pid;
        if(arguments[1]==NULL){
        current_pid = getpid();
        sprintf(arr,"%d",getpid());
        s = arr;
    }
    else{
        s = (arguments[1]);
        current_pid = atoi(s);
    }
        char process_state[] = "ps -o stat= ";
        strcat(process_state,s);
        FILE *fd;
        char path[1500];
        fd = popen(process_state, "r");
        if (fd == NULL) {
            perror("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path, sizeof(path)-1, fd) != NULL) {
       }
        pclose(fd);
        char memory[]="pmap ";
        strcat(memory,s);
        strcat(memory," | grep total");
        char path2[1500];
        fd = popen(memory, "r");
        if (fd == NULL) {
            printf("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path2, sizeof(path2)-1, fd) != NULL) {
        }
        pclose(fd);
        char exe_path[]="/proc/";
        char path3[1500];
        strcat(exe_path,s);
        strcat(exe_path,"/exe");
        int st = readlink(exe_path,path3,1500);
        if(st==-1){
            strcpy(path3,"The link is broken");
        }
        else{
            path3[st]='\0';
        }
        char ** mem = split_input(path2,2);
        printf("pid -- %ld\n",current_pid);
        printf("Process Status -- %s",path);
        printf("memory -- %s {​ Virtual Memory ​ }\n",mem[1]);
        printf("Executable Path --​ %s\n",path3 );
    }
    else if(strcmp(arguments[0],"ls")==0){
        if(strcmp(arguments[1],"-a")==0 || strcmp(arguments[1],"-l")==0){
            ls_cmd(arguments[2],arguments);
        }
        else{
            ls_cmd(arguments[1],arguments);
        }
    }
    else{
    pid_t pid,wpid;
    int status;
    pid = fork();
    if(pid==0){
        execvp(arguments[0],arguments);
        exit(EXIT_FAILURE);} 
    
    else{
        do {
          wpid = waitpid(pid, &status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
        }
    }
    free(arguments);
    }
}
}
int main(int argc, char const *argv[]){
shell_loop();
}