#include "header.h"
int fir;
char temp_inp[500];
char **split_input(char *input,int stage){
strcpy(temp_inp,input);
char *delimiter;
int bufsize = 10;
char **total_inputs = malloc(bufsize*sizeof(char*));
char *current_input;
int it = 0;
if(!total_inputs){
    perror("Memory allocation error");
    exit(1);
}
if(stage==1){
    current_input = strtok(input,";");}
else if(stage==2){
    current_input = strtok(input," \t\r\n\a");}
else if(stage==3){
    current_input = strtok(input," .-\t\r\n\a");}
while(current_input != NULL){
    total_inputs[it++] = current_input;
    if (it >= bufsize) {
      bufsize += 50;
      total_inputs = realloc(total_inputs, bufsize * sizeof(char*));
      if (!total_inputs) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
    if(stage==1){
    current_input = strtok(NULL,";");}
    else if(stage==2){
    current_input = strtok(NULL," \t\r\n\a");}
    else if(stage==3){
    current_input = strtok(NULL," .-\t\r\n\a");}
}
    total_inputs[it]=NULL;
    return total_inputs;
}
void handl(int sig){
            pid_t wpid;
            int status;
            while ((wpid=waitpid(fir,&status,WNOHANG)) > 0)
            {
            if ( WIFEXITED(status)) 
            { 
                int exit_status = WEXITSTATUS(status);         
                printf("Exit status of the child was %d\n",exit_status); 
                printf("%d exited normally\n",fir);
            }
            } 
}
void shell_loop(){
char username[500],systemname[500],curr_dir[500],home[500],print_cwd[500];
char *input=NULL;
char **total_args;
char **arguments;
int it_chdir = -1;
size_t input_length;
ssize_t input_read;
int home_status=0,check_home_currdir;
while(1){
cuserid(username);
gethostname(systemname,sizeof(systemname));
getcwd(curr_dir,sizeof(curr_dir));
int below_home=0;
if(home_status == 0){
strcpy(home,curr_dir);
home_status = 1;
}
check_home_currdir = strcmp(home,curr_dir);
if(check_home_currdir != 0){
    for(int i=0;home[i]!='\0';i++){
        if(home[i]!=curr_dir[i])
        {
			if(strlen(home)>strlen(curr_dir)){
				below_home=1;
				strcpy(print_cwd,curr_dir);
			}            
            break;
        }
            it_chdir = i;
	}
            if(it_chdir!=-1 && below_home==0){
            int count = 1;
            print_cwd[0]='~';
            for(int j=it_chdir+1;curr_dir[j]!='\0';){
                print_cwd[count++] = curr_dir[j++];
            }
            print_cwd[count]='\0';
            it_chdir=-1;
            }
}
else{
    print_cwd[0]='~';
    print_cwd[1]='\0';
}
printf("<%s@%s:%s>",username,systemname,print_cwd);
int background_proc = 0;
input_read = getline(&input,&input_length,stdin);
total_args = split_input(input,1);
history_cmd(input,home);
for(int k=0;total_args[k]!=NULL;k++){ 
    arguments = split_input(total_args[k],2);
    int p = 0;
    if(arguments[p]==NULL){perror("");continue;}
    for(p=0;arguments[p]!=NULL;p++){}
    if(strcmp(arguments[p-1],"&")==0){
        arguments[p-1] = NULL;
        background_proc +=1;
    }
if(input_read!=-1){
    if(strcmp(arguments[0],"cd")==0){
    		if(arguments[1]==NULL){arguments[1]="~";}
            builtin_cd(arguments,username,systemname,home);
        }
    else if(strcmp(arguments[0],"pwd")==0){
        printf("%s\n",curr_dir);
    }
    else if(strcmp(arguments[0],"echo")==0){
        int temp_flag = 0;
        for(int i=0;temp_inp[i]!='\0';i++){
            if(temp_flag==1 && temp_inp[i]!='"'){
                printf("%c",temp_inp[i]);
            }
            if(temp_inp[i]=='"'){
                temp_flag=1;
            }
        }
    }
    else if(strcmp(arguments[0],"pinfo")==0){
        char *s;
        char arr[500];
        long int current_pid;
        if(arguments[1]==NULL){
        current_pid = getpid();
        sprintf(arr,"%d",getpid());
        s = arr;
        }
        else{
            s = (arguments[1]);
            current_pid = atoi(s);
        }
        struct stat sts;
        char proc_check[]= "/proc/";
        strcat(proc_check,s);
        if (stat(proc_check, &sts) == -1) {
            printf("process not found with given pid\n");
        }
        else{
        char process_state[] = "ps -o stat= ";
        strcat(process_state,s);
        FILE *fd;
        char path[1500];
        fd = popen(process_state, "r");
        if (fd == NULL) {
            perror("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path, sizeof(path)-1, fd) != NULL) {
       }
        pclose(fd);
        char memory[]="pmap ";
        strcat(memory,s);
        strcat(memory," | grep total");
        char path2[1500];
        fd = popen(memory, "r");
        if (fd == NULL) {
            printf("Failed to run command\n" );
            exit(1);
        }
        while (fgets(path2, sizeof(path2)-1, fd) != NULL) {
        }
        pclose(fd);
        char exe_path[]="/proc/";
        char path3[1500];
        strcat(exe_path,s);
        strcat(exe_path,"/exe");
        int st = readlink(exe_path,path3,1500);
        if(st==-1){
            strcpy(path3,"The link is broken");
        }
        else{
            path3[st]='\0';
        }
        char ** mem = split_input(path2,2);
        printf("pid -- %ld\n",current_pid);
        if(current_pid==getpid()){
            printf("Process Status -- R\n");
        }else{
            printf("Process Status -- %s\n", path);
        }
        printf("memory -- %s {​ Virtual Memory ​ }\n",mem[1]);
        printf("Executable Path --​ %s\n",path3 );
    }
    }
    else if(strcmp(arguments[0],"ls")==0){
    	if(arguments[1]==NULL){
    		arguments[1]=".";
            ls_cmd(arguments[1],arguments);
    	}
        else if(strcmp(arguments[1],"-a")==0 || strcmp(arguments[1],"-l")==0 || strcmp(arguments[1],"-al")==0 || strcmp(arguments[1],"-la")==0){
            if(arguments[2]==NULL){arguments[2]=".";}
            ls_cmd(arguments[2],arguments);
        }
        else{
            ls_cmd(arguments[1],arguments);
        }
    }
    else if(strcmp(arguments[0],"history")==0){
    	print_history(home,arguments);
    }
    else{
    pid_t pid,wpid;
    int status;
    pid = fork();
    if(pid==0){
        execvp(arguments[0],arguments);
        exit(EXIT_FAILURE);} 
    
    else if(strcmp(arguments[0],"ls")!=0){
        if(background_proc==0){
            wpid = waitpid(pid, &status, WUNTRACED);
        	 if ( WIFEXITED(status)) 
    		{ 
        		int exit_status = WEXITSTATUS(status);         
        		printf("Exit status of the child was %d\n",exit_status); 
                printf("%s with %d exited normally\n",arguments[0],pid);
    		} 
        }
        else{
        	fir = pid;
        	signal(SIGCHLD,handl);
        }
    }
    free(arguments);
    }
}
}
}
}