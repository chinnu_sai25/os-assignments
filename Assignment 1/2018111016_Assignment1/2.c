#include <stdio.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <fcntl.h> 
#include<stdlib.h>
#include <sys/stat.h> 
char old_arr[9001110],new_arr[9001110];
long long int dir_exists,new_exists,old_exists;
char out[]="Directory is created: Yes\n";
char nout[]="Directory is created: No\n";
char nusrr[]="User has read permissions on newfile: Yes\n";
char nusrnr[]="User has read permissions on newfile: No\n";
char nusrw[]="User has write permission on newfile: Yes\n";
char nusrnw[]="User has write permission on newfile: No\n";
char nusre[]="User has execute permission on newfile: Yes\n\n";
char nusrne[]="User has execute permission on newfile: No\n\n";
char ngrpr[]="Group has read permissions on newfile: Yes\n";
char ngrpnr[]="Group has read permissions on newfile: No\n";
char ngrpw[]="Group has write permission on newfile: Yes\n";
char ngrpnw[]="Group has write permission on newfile: No\n";
char ngrpe[]="Group has execute permission on newfile: Yes\n\n";
char ngrpne[]="Group has execute permission on newfile: No\n\n";
char nothr[]="Others has read permissions on newfile: Yes\n";
char nothnr[]="Others has read permissions on newfile: No\n";
char nothw[]="Others has write permission on newfile: Yes\n";
char nothnw[]="Others has write permission on newfile: No\n";
char nothe[]="Others has execute permission on newfile: Yes\n\n";
char nothne[]="Others has execute permission on newfile: No\n\n";

char ousrr[]="User has read permissions on oldfile: Yes\n";
char ousrnr[]="User has read permissions on oldfile: No\n";
char ousrw[]="User has write permission on oldfile: Yes\n";
char ousrnw[]="User has write permission on oldfile: No\n";
char ousre[]="User has execute permission on oldfile: Yes\n\n";
char ousrne[]="User has execute permission on oldfile: No\n\n";
char ogrpr[]="Group has read permissions on oldfile: Yes\n";
char ogrpnr[]="Group has read permissions on oldfile: No\n";
char ogrpw[]="Group has write permission on oldfile: Yes\n";
char ogrpnw[]="Group has write permission on oldfile: No\n";
char ogrpe[]="Group has execute permission on oldfile: Yes\n\n";
char ogrpne[]="Group has execute permission on oldfile: No\n\n";
char oothr[]="Others has read permissions on oldfile: Yes\n";
char oothnr[]="Others has read permissions on oldfile: No\n";
char oothw[]="Others has write permission on oldfile: Yes\n";
char oothnw[]="Others has write permission on oldfile: No\n";
char oothe[]="Others has execute permission on oldfile: Yes\n\n";
char oothne[]="Others has execute permission on oldfile: No\n\n";

char dusrr[]="User has read permissions on directory: Yes\n";
char dusrnr[]="User has read permissions on directory: No\n";
char dusrw[]="User has write permission on directory: Yes\n";
char dusrnw[]="User has write permission on directory: No\n";
char dusre[]="User has execute permission on directory: Yes\n\n";
char dusrne[]="User has execute permission on directory: No\n\n";
char dgrpr[]="Group has read permissions on directory: Yes\n";
char dgrpnr[]="Group has read permissions on directory: No\n";
char dgrpw[]="Group has write permission on directory: Yes\n";
char dgrpnw[]="Group has write permission on directory: No\n";
char dgrpe[]="Group has execute permission on directory: Yes\n\n";
char dgrpne[]="Group has execute permission on directory: No\n\n";
char dothr[]="Others has read permissions on directory: Yes\n";
char dothnr[]="Others has read permissions on directory: No\n";
char dothw[]="Others has write permission on directory: Yes\n";
char dothnw[]="Others has write permission on directory: No\n";
char dothe[]="Others has execute permission on directory: Yes\n\n";
char dothne[]="Others has execute permission on directory: No\n\n";

void print(char filename[],int type){
long long int s=open(filename,O_RDONLY);
if(type==4){
struct stat sb;
if (stat(filename, &sb) == 0 && S_ISDIR(sb.st_mode))
{
    write(1,out,sizeof(out));
    dir_exists=1;
}
else{
    write(1,nout,sizeof(nout));
    perror("error");
    dir_exists=0;
}
}
else{
struct stat sb;
stat(filename,&sb);
if(sb.st_mode & S_IRUSR){
    if(type==1)write(1,nusrr,sizeof(nusrr));
    if(type==2)write(1,ousrr,sizeof(ousrr));
    if(type==3)write(1,dusrr,sizeof(dusrr));
}
else{
    if(type==1)write(1,nusrnr,sizeof(nusrnr));
    if(type==2)write(1,ousrnr,sizeof(ousrnr));
    if(type==3)write(1,dusrnr,sizeof(dusrnr));
}
if(sb.st_mode & S_IWUSR){
    if(type==1)write(1,nusrw,sizeof(nusrw));
    if(type==2)write(1,ousrw,sizeof(ousrw));
    if(type==3)write(1,dusrw,sizeof(dusrw));
}  
else{
    if(type==1)write(1,nusrnw,sizeof(nusrnw));
    if(type==2)write(1,ousrnw,sizeof(ousrnw));
    if(type==3)write(1,dusrnw,sizeof(dusrnw));
}
if(sb.st_mode & S_IXUSR){
    if(type==1)write(1,nusre,sizeof(nusre));
    if(type==2)write(1,ousre,sizeof(ousre));
    if(type==3)write(1,dusre,sizeof(dusre));
}
else{
    if(type==1)write(1,nusrne,sizeof(nusrne));
    if(type==2)write(1,ousrne,sizeof(ousrne));
    if(type==3)write(1,dusrne,sizeof(dusrne));
}
if(sb.st_mode & S_IRGRP){
    if(type==1)write(1,ngrpr,sizeof(ngrpr));
    if(type==2)write(1,ogrpr,sizeof(ogrpr));
    if(type==3)write(1,dgrpr,sizeof(dgrpr));
}
else{
    if(type==1)write(1,ngrpnr,sizeof(ngrpnr));
    if(type==2)write(1,ogrpnr,sizeof(ogrpnr));
    if(type==3)write(1,dgrpnr,sizeof(dgrpnr));
}
if(sb.st_mode & S_IWGRP){
    if(type==1)write(1,ngrpw,sizeof(ngrpw));
    if(type==2)write(1,ogrpw,sizeof(ogrpw));
    if(type==3)write(1,dgrpw,sizeof(dgrpw));
}  
else{
    if(type==1)write(1,ngrpnw,sizeof(ngrpnw));
    if(type==2)write(1,ogrpnw,sizeof(ogrpnw));
    if(type==3)write(1,dgrpnw,sizeof(dgrpnw));
}
if(sb.st_mode & S_IXGRP){
    if(type==1)write(1,ngrpe,sizeof(ngrpe));
    if(type==2)write(1,ogrpe,sizeof(ogrpe));
    if(type==3)write(1,dgrpe,sizeof(dgrpe));
}
else{
    if(type==1)write(1,ngrpne,sizeof(ngrpne));
    if(type==2)write(1,ogrpne,sizeof(ogrpne));
    if(type==3)write(1,dgrpne,sizeof(dgrpne));
}
if(sb.st_mode & S_IROTH){
    if(type==1)write(1,nothr,sizeof(nothr));
    if(type==2)write(1,oothr,sizeof(oothr));
    if(type==3)write(1,dothr,sizeof(dothr));
}
else{
    if(type==1)write(1,nothnr,sizeof(nothnr));
    if(type==2)write(1,oothnr,sizeof(oothnr));
    if(type==3)write(1,dothnr,sizeof(dothnr));
}
if(sb.st_mode & S_IWOTH){
    if(type==1)write(1,nothw,sizeof(nothw));
    if(type==2)write(1,oothw,sizeof(oothw));
    if(type==3)write(1,dothw,sizeof(dothw));
}  
else{
    if(type==1)write(1,nothnw,sizeof(nothnw));
    if(type==2)write(1,oothnw,sizeof(oothnw));
    if(type==3)write(1,dothnw,sizeof(dothnw));
}
if(sb.st_mode & S_IXOTH){
    if(type==1)write(1,nothe,sizeof(nothe));
    if(type==2)write(1,oothe,sizeof(oothe));
    if(type==3)write(1,dothe,sizeof(dothe));
}
else{
    if(type==1)write(1,nothne,sizeof(nothne));
    if(type==2)write(1,oothne,sizeof(oothne));
    if(type==3)write(1,dothne,sizeof(dothne));
}
}
close(s);
}
int main(int argc,char *argv[]){
    if(argc!=4){perror("error");}
    int old_file = open(argv[2],O_RDONLY);
    print(argv[3],4);
    long long int new_file = open(argv[1],O_RDONLY);
    long long int n = 9000010;
    long long int count=0,flag = 0,cnt = 0,checker=0;
    char outputc[] = "Whether file contents are reversed in newfile: Yes\n\n";
    char outputw[] = "Whether file contents are reversed in newfile: No\n\n";
    long long int total = lseek(new_file,0,SEEK_END);
    long long int b = lseek(new_file,-n,SEEK_END);
    if(b>=0){
    while(read(old_file,old_arr,9000010) && read(new_file,new_arr,9000010) && flag==0 && checker>=0)
    {   
        cnt++;
        for(long long int i=0;i<9000010;i++){
            if(old_arr[i]!=new_arr[9000010-1-i]){
                flag = 1;
                break;
            }
        }
        n = n+9000010;
        checker = lseek(new_file,-n,SEEK_END);
    }
    if(total-cnt*9000010>0 && flag==0){
        lseek(old_file,-(total-cnt*9000010),SEEK_END);
        lseek(new_file,0,SEEK_SET);
        read(old_file,old_arr,total-cnt*9000010);
        read(new_file,new_arr,total-cnt*9000010);
        for(long long int i=0;i<total-cnt*9000010;i++){
            if(old_arr[i]!=new_arr[(total-cnt*9000010)-1-i]){
                flag = 1;
                break;
            }
        }
    }
    }
    else if(b<0){
        lseek(old_file,0,SEEK_SET);
        lseek(new_file,0,SEEK_SET);
        read(old_file,old_arr,total);
        read(new_file,new_arr,total);
        for(long long int i=0;i<total;i++){
            if(old_arr[i]!=new_arr[total-1-i]){
                flag = 1;
                break;
            }
        }
    }
    if(new_file!=-1  && old_file!=-1){
    long long int total1 = lseek(old_file,0,SEEK_END);
    if(flag==0 && total==total1)
    {
        write(1,outputc,sizeof(outputc));
    }
    else{
        write(1,outputw,sizeof(outputw));
    }
    }
    else{
        perror("error");
    }
    if(new_file!=-1)print(argv[1],1);
    if(old_file!=-1)print(argv[2],2);
    if(dir_exists)print(argv[3],3);
    close(new_file);
    close(old_file);
}